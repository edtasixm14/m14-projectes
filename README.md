# M14-Prjectes

## @edt ASIX M14 2022-2023

Podeu trobar les imatges docker al Dockehub de [edtasixm14](https://hub.docker.com/u/edtasixm14/)

Podeu trobar la documentació i els dokerfiles del mòdul a [ASIX-M14](https://github.com/edtasixm14)


[ASIX-M01](https://gitlab.com/edtasixm01/m01-operatius)  |  [ASIX-M05](https://gitlab.com/edtasixm05/m05-hardware) |  [ASIX-M06](https://gitlab.com/edtasixm06/m06-aso) |  [ASIX-M11](https://gitlab.com/edtasixm11/m11-sad)

---


**Documentació:**

 * [m14-projectes_reptes.pdf](https://gitlab.com/edtasixm14/m14-projectes/-/blob/master/m14-projectes_reptes.pdf)  Guió del mòdul de projectes amb el llistat dels temes i els reptes associats a cada tema.


---


**Descripció:**


Projecte Trimestral Primer Trimestre

 * Fase 1: Autenticació LDAP amb homes tmpfs
 * Fase 2: Servei DNS i monitorització amb Grafana
 * Fase 3 Muntatge de homes dels usuaris amb NFS
 * Fase 4 Muntatge de homes dels usuaris amb SSHFS

Projecte trimestral Segon Trimestre

 * Fase 1: API REST web amb autenticació OAUTH
 * Fase 2: Servidor web amb seus virtuals
 * Fase 3: Autenticació web amb LDAP
 * Fase 4: AWS VPC infrastructure


Projecte Trimestral tercer trimestre / FINAL

 * Fase 1: Implementació d’un SAMBA AD
 * Fase 2: Implementació del servei FreeIPA
 * Fase 3: Múltiples dominis, confiança i federació
 * Fase 4: Serveis avançats

El projecte final a desenvolupar aquest tercer trimestre és un projecte global que barca tots els coneixements adquirits al  cicle i que representa un repte d’implementació d’un projecte real d’administració de sistemes informàtics. Aquest repte consisteix en implementar el desplegament i administració del sistema informàtic d’una escola com per exemple l’Escola del Treball de Barcelona, en el nostre cas la coneguda edt.org.

 * Implemntar un sistema d'autenticació usant teconologies: LDAP, PAM, Kerberos i també FreeIPA.
 * Implementar sistemes de homes d'usuaris basats en: NFS, SSHFS i SAMBA.
 * Implementar scripts d'administració del sistema i d'administració d'usuaris.
 * Aplicar Containers i desplegaments al cloud usant Docker i IaaS (per exemple AWS).
 * Utilitzar eines de desplegament com Vagrant, Ansible, Packer i Terraform.



