#! /bin/bash
# Install automated F2G

apt-get -y install vim vim-gtk3 mc geany aptitude tree git openssh-server && systemctl disable ssh
apt-get purge gnome-games -y && apt-get autoremove -y
DEBIAN_FRONTEND=noninteractive apt-get install krb5-user krb5-multidev libpam-mount sssd nfs-common autofs ufw curl -y

git clone https://gitlab.com/manelmellado/ubnt-at-inf.git
cd ubnt-at-inf/Debian\ 11/arxius/etc/
sudo cp sssd/sssd.conf /etc/sssd/
sudo cp sudoers.d/inf /etc/sudoers.d/
chmod 600 /etc/sssd/sssd.conf
#pam-auth-update
sudo pam-auth-update --enable mkhomedir sss systemd pwquality unix libpam-mount

systemctl disable sssd-nss.socket
systemctl disable sssd-autofs.socket
systemctl restart sssd
systemctl restart rpc-gssd

# Editar fstab

apt-get -y install gpm pwgen
sed -i '/^[^#]/  s/main$/main contrib non-free/' /etc/apt/sources.list
apt-get update
apt-get -y install chromium vlc gimp git tig meld linux-headers-`uname -r` gnome-shell-extension-desktop-icons terminator firmware-realtek firmware-misc-nonfree hunspell-ca hunspell-es

AULA=profeF2G
SERVER=10.200.247.246
line="$SERVER $AULA.informatica.escoladeltreball.org server"
sudo bash -c "echo $line >> /etc/hosts"
apt-get -y install --install-recommends epoptes-client -y
epoptes-client -c  $AULA.informatica.escoladeltreball.org

apt-mark hold linux-image-$(uname -r) # per apt-get
aptitude hold linux-image-$(uname -r) # per aptitude
apt-mark hold linux-headers-$(uname -r) # per apt-get
aptitude hold linux-headers-$(uname -r) # per aptitude
sed -i 's/\#WaylandEnable=false/WaylandEnable=false/' /etc/gdm3/daemon.conf

