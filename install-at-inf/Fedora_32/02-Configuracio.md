# Configuració de Linux Fedora-32

# Curs 2020-2021

Els enllaços a aquesta documentació i els seus repositoris es poden consultar en la pàgina
<http://tinyurl.com/fed-at-inf/documents>.

## Punts clau on posar atenció

* És obligatori seguir sempre l’ordre de les operacions presentades.
* Si el sistema no ens ofereix iniciar sessió com a _guest_ és que no hem
  creat correctament aquest usuari: per arreglar-ho anem a una consola, creem
  l&rsquo;usuari amb les ordress `adduser` i `passwd` i reiniciem el sistema
  amb `reboot`.

## Configuració

1. Iniciem sessió gràfica com l’usuari _guest_. Obrim el _Firefox_ i un terminal;
   disposem les finestres per veure-les simultàniament.
    * En el navegador visualitzem les instruccions en la pàgina de
      **Configuració** dins del repositori de _Git_ mencionat més amunt.
    * En el terminal ens convertim en administrador executant `su -l`.

    Notació: a partir d’ara si veiem el caràcter `#` davant d’una ordre que hem
    d’executar voldrà dir que per escriure-la haurem de ser l’usuari _root_ (el _super
    usuari_ o administrador). I aquest caràcter `#` no l’hem de posar.

2. Configurem serveis apropiadament:

    ```
    # systemctl stop firewalld.service
    # systemctl disable firewalld.service
    # systemctl stop sssd.service
    # systemctl stop nfs-utils.service
    ```

3. En comptes de fer servir l’editor vi farem servir vim (life is colorful!).

    L’instal·lem com a _root_:

    ```
    # dnf -y install vim gvim mc geany
    ```

4. Desactivem el _selinux_:

    Editem el fitxer de configuració de _selinux_ modificant l’opció que diu
    `enforcing` i la canviem per `permissive`.  Atenció: el fitxer mal editat
    implica **tornar** a instal·lar.

    ```
    # setenforce 0
    # vim /etc/selinux/config
    ```

    Premem la tecla **i** per inserir contingut en el fitxer.  Per desar cal
    prémer la tecla **ESC** per entrar en mode ordre i llavors escriure `:wq`.

        SELINUX=permissive

    Alternativament, per anar més ràpid i evitar errors manuals:

    ```
    # sed -i -e s,'SELINUX=enforcing','SELINUX=permissive', /etc/selinux/config
    ```


5. Configuració de la informació d'usuari (LDAP) i autenticació (Kerberos)

    Necessitem instal·lar certs paquets per la configuració de Kerberos:

    ```
    # dnf -y install  krb5-devel krb5-workstation
    # dnf -y install pam_mount
    ```

    Si hem llegit fins a aquesta línia tenim premi, el mateix d’abans en una línia:

    ```
    # dnf -y install  krb5-devel krb5-workstation pam_mount
    ```


6. Descarreguem l’arxiu de configuracions i el descomprimim:

    ```
    # cd /tmp
    # curl ftp://ftp/pub/deploy.sh | bash
    ```
    Reiniciem els serveis que afecten a l’autenticació en Kerberos i al NFS:

    ```
    # systemctl start sssd.service
    # systemctl start nfs-utils.service
    # systemctl restart nfs-client.target
    ```

7. Ara editarem el fitxer `/etc/fstab`.

    Editem el fitxer /etc/fstab amb vim:
    ```
    # vim /etc/fstab
    ```
    Premem la tecla **i** per inserir contingut en el fitxer.

    Editem el fitxer `/etc/fstab` i substituïm els `UUID` o `LABEL` de la
    partició arrel ( _/_ ) i de la partició _swap_. Posar-hi en el seu lloc el *device*
    pertinent: `/dev/sda5` per la partició arrel del matí o `/dev/sda6` per la
    partició arrel de la tarda,  i `/dev/sda7` per a la de _swap_.

    On diu, per exemple:

    ```
    UUID=236896b6-5701-4411-a2e8-96442c3d4725  /     ext4 defaults  1 1
    UUID=151174f5-5652-4b02-af4b-942f2a70d31a  swap  swap defaults  0 0
    ```

    Ha de dir, en el cas del matí:

    ```
    /dev/sda5    /       ext4    defaults        1 1  #/dev/sda6 a la TARDA
    /dev/sda7  swap      swap    defaults        0 0
    ```

    A continuació afegim la línia següent al final de tot:

    ```
    gandhi:/groups/ /home/groups  nfs4  sec=krb5,comment=systemd.automount  0  0
    ```
    Per desar cal prémer la tecla **ESC** per entrar en mode ordre i llavors escriure **:wq**.
    
    Per muntar i comprovar que s’han muntat els sistemes de fitxers fem:

    ```
    # mount -a
    ```

    Si hi ha errors serà perquè alguna línia no s'ha escrit bé.

8. ~~Excloem futures actualitzacions del kernel per evitar complicacions amb el
   GRUB afegint una línia al final del fitxer `/etc/dnf/dnf.conf `.~~

    ```
    # echo 'exclude=kernel-5*,kernel-core,kernel-modules' >> /etc/dnf/dnf.conf
    ```

9. Ara cal reiniciar el sistema executant en el terminal l’ordre `reboot`. **Mai** s’apaga un sistema Linux directament
   (cal usar `poweroff`, `shutdown` o `reboot`).

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

**Final configuració**

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

Seguim amb les instruccions del fitxer sobre [Personalització](03-Personalitzacio.md):
caldrà iniciar sessió gràfica amb el teu usuari i obrir el _Firefox_ per poder
consultar el fitxer [Personalització](03-Personalitzacio.md).

<!--
vim:ts=4:sw=4:ai:et:fileencoding=utf8:syntax=markdown
-->
