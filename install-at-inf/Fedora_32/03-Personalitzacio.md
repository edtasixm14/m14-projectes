# Personalització de Linux Fedora-32

# Curs 2020-2021

Els enllaços a aquesta documentació i els seus repositoris es poden consultar en la pàgina
<http://tinyurl.com/fed-at-inf/documents>.

## Punts clau on posar atenció

* Quan posem la nostra contrasenya d’usuari hem de vigilar de no usar el teclat
  **numèric**. També cal verificar de no prèmer la tecla **Bloq Mayús**.

## Personalització

1. Iniciem sessió gràfica amb el nostre propi compte d’usuari.  Verifiquem en un
   terminal l’accés al compte d’usuari modificant la contrasenya. Cal que
   compleixi unes determinades normes de longitud i combinació de lletres i
   xifres.
   
    ```
    $ passwd
    ```

2. Ens posem com a usuari _root_ (amb l’ordre `su -l` ) i instal·lem el servei `gpm`; el configurarem per tenir-lo sempre disponible:

    ```
    $ su -l
    # dnf -y install gpm
    # systemctl enable gpm.service
    # systemctl start gpm.service  
    ```

    Podem també assegurar el compte de convidat (*guest*) amb l’ordre:

    ```
    # dnf -y install pwgen
    # pwgen -1 | passwd --stdin guest
    ```

3.  Repositoris de tercers.

    RPM Fusion:

    ```
    # rpm -ivh http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-32.noarch.rpm
    # rpm -ivh http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-32.noarch.rpm
    ```

    Adobe flash plugin per Firefox:

    ```
    # dnf -y install http://linuxdownload.adobe.com/adobe-release/adobe-release-x86_64-1.0-1.noarch.rpm
    # rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-adobe-linux
    # dnf -y install flash-plugin
    ```

    Verifiquem la llista actual de repositoris:

    ```
    # dnf repolist
    ```

4. Programari extra.

    Instal·lem navegador, reproductor multimèdia, editor d'imatges, programari
    de control de versions, configuració d'escriptori Gnome i els headers del
    kernel necessaris per desenvolupament:

    ```
    # dnf install chromium vlc gimp git tig gnome-tweak-tool gnome-shell-extension-desktop-icons kernel-devel -y
    ```

    Deixem de ser _root_ per tornar a l’usuari personal amb l’ordre:

    ```
    # exit
    ```

5. Espai en disc.

    Tots els usuaris disposem d'una quota de 100 MB al servidor de departament. Per saber l’espai ocupat i controlar possibles fitxers massa grans podem executar l'ordre:
    ```
    $ du -h --max-depth=1 $HOME/$USERNAME 
    ```

6. Per desactivar les actualitzacions automatiques del sistema en el nostre
   perfil d’usuari cal executar:

    ```
    $ gsettings set org.gnome.software download-updates false
    ```

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

**Final personalització**

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

## Procediments de verificació

* Funcionament correcte de Kerberos: simplement iniciem sessió en una
  consola  amb el compte personal d’usuari (anem a la consola prement **Ctrl+Alt+F3**).
    
* Els directoris del servidor estan correctament exportats?

    ```
    $ mount | grep gandhi
    ```

* Verificació dels repositoris externs:

    ```
    $ dnf repolist
    ```

* Verificació de l’estat del _selinux_:

    ```
    $ getenforce
    ```

* Hi ha programari que no es pot instal·lar, com per exemple *vlc*?

    Comprova que la instal·lació prèvia dels repositoris de *rpmfusion* s'hagi executat correctament.

* Tinc problemes de gràfics amb cert programari (virtualització, etc)

    En aquesta versió de Fedora es fa servir per defecte *Wayland* en comptes de *Xorg*, prova de sortir de la sessió i entrar amb *Xorg*.

* No veus les icones a l'escriptori?

    Assegura't que has instal·lat el paquet *gnome-shell-extension-desktop-icons* i després pots configurar-ho des de la pestanya de les extensions a *gnome-tweak-tool*. O si vols des de la consola:

    ```
    gnome-extensions enable desktop-icons
    ```

<!--
vim:ts=4:sw=4:ai:et:fileencoding=utf8:syntax=markdown
-->
