# Com trobar quin paquet a Debian conté un programa concret


## Problema

Volem fer servir un programa que no tenim instal·lat:

```
virt-builder
bash: virt-builder: command not found
```

En aquest cas, no coincideix el nom del programa amb el del paquet que el
conté.


## Solució

Per esbrinar quin és el paquet que conté un programa concret podem
instal·lar-nos una eina molt útil: `apt-file`

```
apt-get install apt-file
```

La primera vegada la caché podria estar buida, de manera que val la pena fer:

```
sudo apt-file update
```

A partir d'aquest moment ja podrem fer servir aquest programa, en particular
per resoldre el nostre problema:

```
apt-file search virt-builder
```

També funciona amb `find` en comptes de `search`


De l'informació que mostra l'ordre anterior es dedueix que el paquet és `libguestfs-tools` de manera que l'únic que haurem de fer és:

```
apt-get install libguestfs-tools
```


## Equivalències a Fedora / Centos

A Fedora podríem fer:

```
dnf provides '*virt-builder'
```

Mentre que a Centos seria:

```
yum whatprovides '*virt-builder'
```

