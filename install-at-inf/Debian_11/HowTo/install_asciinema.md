# Com instal·lar asciinema

> asciinema és una eina per gravar sessions de consola i compartir-la via web
> (o reproduir-la amb la mateixa eina)


## Instal·lació

```
sudo apt-get install asciinema
```

## Gravació

```
asciinema play nom_fitxer.cast
```


## Reproduir-lo localment

```
asciinema rec nom_fitxer.cast
```

## Compartir-lo al web

+ Creem un compte simplement afegint una adreça de correu i confirmant-la en
  aquest [enllaç](https://asciinema.org/login/new).

+ Enllacem tots les gravacions que fem en aquesta màquina al nostre compte:

	```
	asciinema auth
	```
	
	Cliquem l'enllaç que ens surt a la consola i ja tenim associats tots els
vídeos en aquesta màquina al nostre compte.

	Més info: `man asciinema` (secció auth)

+ El pugem amb l'ordre:

	```
	asciinema upload nom_fitxer.cast
	```
	Ens proporcionarà un link i voilà

## Exemple

Podeu veure un exemple molt bàsic en [aquest enllaç](https://asciinema.org/a/7bShlFafA67JwoVDR47juaIFW)
