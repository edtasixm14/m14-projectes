# Instal·lació d'Autofirma a Debian


## Concepte

És important distingir entre la signatura digital d'un document, que no té perquè fer-se visible al document i l'opció que tenim d'afegir un text per "visualitzar" aquesta firma.

La comprovació real de la signatura es fa mitjançant alguna eina alternativa, com la comentada posteriorment en aquest document: `pdfsign`.


## Descarrega del software

Descarreguem [d'aquest web](https://firmaelectronica.gob.es/Home/Descargas.html) el paquet per a Linux:

```
wget https://estaticos.redsara.es/comunes/autofirma/1/7/1/AutoFirma_Linux.zip
```

Descomprimim:

```
unzip AutoFirma_Linux.zip
```

Es mostrarà uns missatges semblants a:

```
Archive:  AutoFirma_Linux.zip
  inflating: AF_manual_instalacion_usuarios_ES.pdf  
 extracting: autofirma-1.7.1-1.noarch_FEDORA.rpm  
 extracting: AutoFirma_1_7_1.deb     
  inflating: autofirma-1.7.1-1.noarch_SUSE.rpm 
```

De tots els paquets per a diferents distribucions GNU/Linux ens interessa el paquet _deb_

L'instal·lem:

```
sudo apt-get install ./AutoFirma_1_7_1.deb
```

Ja podem fer-lo servir de manera gràfica si volem

Un cop s'hagi signat amb el nostre certificat electrònic podem comprovar que s'ha signat correctament amb l'ordre:

```
pdfsig nom_del_fitxer_signat
```

Et voilà


## Mode consola

Es pot treballar des de la línia d'ordres.

Per exemple la següent ordre signa afegint un text al marge esquerra de la darrera pàgina del document i en orientació vertical.

```
java -jar /usr/lib/AutoFirma/AutoFirma.jar sign -i input.pdf -o output_signed.pdf -filter subject.contains:el_teu_DNI_per_exemple -config 'signaturePage=last\nsignaturePositionOnPageUpperRightX=70\nsignaturePositionOnPageUpperRightY=600\nsignaturePositionOnPageLowerLeftX=10\nsignaturePositionOnPageLowerLeftY=10\nlayer2FontFamily=0\nlayer2FontSize=8\nsignatureRotation=90\nlayer2Text=Firmat per $$SUBJECTCN$$ el dia $$SIGNDATE=dd/MM/yyyy$$ amb certificat emès per $$ISSUERCN$$'
```

Totes les opcions [les podem trobar al codi de l'aplicació](https://github.com/ctt-gob-es/clienteafirma/blob/3cf50b77228e47984e0c83809210bfd2fbb5d98c/afirma-simple/src/main/resources/help/Spanish.lproj/pgs/LineaComandos.html)


## Mode gràfic

De vegades pot haver algun problema amb la versió / tipus de pdf. Una possible solució és omplir les dades necessàries del pdf, excepte la signatura, i **imprimir amb sortida a fitxer pdf**. Això ens genera un nou document pdf que sí que es pot signar amb `AutoFirma`.

És possible que llavors no detecti el camp reservat per a escriure un text de firma. En aquest cas el mateix programa ens demanar que definim l'àrea pel text allà a on vulguem. Ja sigui donant nosaltres les coordenades del rectangle del text o de forma més senzilla fent servir el ratolí.

## Comprovació

Recordem que el document està signat digitalment, no és necessari escriure cap text.
Una manera de comprovar en linux que s'ha signat és fer servir l'ordre `pdfsign`:

```
pdfsign document_signat.pdf

Digital Signature Info of: document_signat.pdf
Signature #1:
  - Signer Certificate Common Name: PEP MAYNOU GARCIA - DNI 12345678Z
  - Signer full Distinguished Name: CN=PEP MAYNOU GARCIA - DNI 12345678Z,serialNumber=IDCES-12345678Z,givenName=PEP,SN=MAYNOU GARCIA,C=ES
  - Signing Time: May 08 2022 04:15:06
  - Signing Hash Algorithm: SHA-512
  - Signature Type: adbe.pkcs7.detached
  - Signed Ranges: [0 - 255], [54257 - 162229]
  - Total document signed
  - Signature Validation: Signature is Valid.
...
```
