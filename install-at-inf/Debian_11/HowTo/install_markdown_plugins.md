## PLUGINS MARKDOWN

_Si es vol llegir fitxers en format markdown a un navegador s'ha d'afegir un plugin (addon o extension)_

#### Firefox

A firefox quantum, es pot escollir entre diferents plugins. Un d'aquests és _Markdown Viewer Webext_

Un cop instal·lat permet veure les pàgines remotes fetes en markdown però no les locals. _Que faríem sense aquests bugs?_

Des d'una terminal s'han d'executar les següents instruccions (que només afectaran el nostre usuari):

```
mkdir -p ~/.local/share/mime/packages/
vim ~/.local/share/mime/packages/text-markdown.xml
```

i s'afegeix dintre:

```
<?xml version="1.0"?>
<mime-info xmlns='http://www.freedesktop.org/standards/shared-mime-info'>
  <mime-type type="text/plain">
    <glob pattern="*.md"/>
    <glob pattern="*.mkd"/>
    <glob pattern="*.markdown"/>
  </mime-type>
</mime-info>
```

i finalment actualitzem:

```
update-mime-database ~/.local/share/mime
```

(extret de superuser )


#### Chromium

Chromium també permet escollir entre diferents extensions. 

* Instal·lem Markdown Preview Plus

* Un cop instal·lat, per poder llegir fitxers locals hem d'activar una opció extra:
	- A la configuració de chromium (normalment els 3 punts verticals que es troben a la cantonada superior dreta) fem "More tools" -> Extensions
	- Reconeixem l'extensió que acabem d'instal·lar i premem el botó "Details".
	- Finalment activem l'opció "Allow access to file URLs"




![acudit sobre els formats](imatges/invisible_formatting.png)
