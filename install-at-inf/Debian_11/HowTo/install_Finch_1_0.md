# Detecció i configuració robot Finch 1.0 a Debian

## Objectiu

Aconseguir que el robot Finch 1.0 funcioni a un sistema Debian estable de manera que qualsevol usuari sense ser root ni necessitat d'utilitzar `sudo` el pugui fer servir.

## Execució script 

Com a usuari _root_ descarregueu-vos d'[aquí](scripts/configure_finch_1.sh) l'script de configuració.

Un cop us situeu al directori on heu descarregat l'script, el podeu executar directament amb l'ordre `bash`:

```
bash configure_finch_1.sh
```

Alternativament podeu donar permís d'execució i executar com un programa executable:

```
chmod u+x configure_finch_1.sh
./configure_finch_1.sh
```

Un cop executat l'script, si no hi ha cap error ja es pot utilitzar l'script

## Troubleshooting

Alguns dels problemes habituals intenten ser recollits a l'script.

+ Hem afegit l'usuari que ha de fer desenvolupament al grup `plugdev`?

	Com a root hem d'afegir l'usuari USERNAME a `plugdev`:

	```
	gpasswd -a USERNAME plugdev
	```

	on USERNAME és el nom del desenvolupador.

+ Hem executat l'script amb el robot connectat?

	Hauríem de desconnectar el cable USB i tornar-lo a connectar.

D'altres problemes podrien estar relacionats amb els ports/connectors USB.

+ Si el port USB del teu ordinador és molt modern Versió3+ podria no funcionar. Prova amb un altre port.

## Links

+ [Projecte Java des de la línia d'ordres](https://www.birdbraintechnologies.com/downloads/finch/FinchJavaCommandLine.zip)

+ [Howto des de la línia d'ordres](https://www.birdbraintechnologies.com/finch1/command-line-java-compilation/)

+ [Projecte Java amb eclipse](https://www.birdbraintechnologies.com/downloads/finch/FinchJavaEclipse.zip)

+ [Howto amb eclipse](https://www.birdbraintechnologies.com/finch1/eclipse/)

+ [Howto les regles udev](https://www.birdbraintechnologies.com/finch1/udev-rules/)

