## Instal·lació de Debian Buster (10.8) a un portàtil Lenovo ThinkBook 14 IIL Intel Core i3-1005G1/8GB/256GB SSD/14"


### Imatge de la instal·lació

Podem escollir diferents imatges de Debian en funció del tipus d'instal·lació, arquitectura, etc ...

També es pot donar el cas de que el nostre portàtil tingui algun element de
maquinari que necessiti firmware propietari. Per si de cas és el nostre cas,
escollirem aquesta opció:
 
Baixem la imatge estable de Debian:

```
wget https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/amd64/iso-cd/firmware-10.8.0-amd64-netinst.iso
```

### Creació del pen USB arrencable

Utilitzarem un pen USB (com s'ha escollit una imatge de menys d'1 GB qualsevol
memòria flash d'aquesta mida serà suficient.). Evidentment, tot el que hi ha hagi al pen USB serà eliminat.

Ens col·loquem al directori a on es troba la imatge *iso* que ens hem baixat i
esbrinem quina és la lletra del nostre dispositiu pen USB. Un cop l'hem
"punxat" mirem si es tracta de la `/dev/sdb` o `/dev/sdc` ....

Suposem que és /dev/sdb copiarem la imatge i sincronitzarem amb les següents ordres com a usuari root:

```
cat firmware*.iso > /dev/sdb 	# pot trigar una estona
sync				# pot trigar una estona
```

### Configuració de la BIOS:

Entrem a la BIOS, amb F1 i si hi demana un password, com per exemple podria ser
*3duc4c102020* el posem.

Qué podem fer a la BIOS?

+ Eliminar el password de la BIOS ( *Security* -> *Set Administrator Password* ->
  *Enter* -> *Current Password* -> Finalment repetim passwords buits.
+ Deshabilitar el *Secure Boot* a *Security*.
+ Habilitar *USB Boot* a *Boot*.

Sortirem de la BIOS desants els canvis. Recordem que F12 ens mostra un menú
d'arrencada per escollir que arrenqui del nostre pen USB a on es troba la
imatge de Debian. 

### Instal·lació de Debian

Farem una *Graphical Install*

+ Seleccionem llengua d'instal·lació *English*.
+ Seleccionem com a *locale* *Spain*.
+ Seleccionem com a teclat *Catalan*.


Hem d'afegir el següent:

The 10th gen CPUs from Intel need the newer 5-series kernels. You need to install the latest kernel from Buster backports in order to get the graphic desktop to run, as well as the firmware for it. You can install those from the recovery tty. My 10th gen NUC would not boot to a console until I installed those.
extret de
https://www.linuxquestions.org/questions/linux-newbie-8/cannot-run-in-framebuffer-mode-please-specify-busids-debian-buster-4175682758/



https://forums.lenovo.com/t5/ThinkBook-Notebooks/Thinkbook-14-IIL-trackpad-not-supported-on-Linux/m-p/5013867?page=4

instal·lar kernels més nous a debian stable amb backports, així no cal utilitzar debian testing:
https://www.sololinux.es/como-actualizar-el-kernel-de-debian-10-y-lmde-4/


recordar d'afegir *contrib* i *non-free* a banda de *main* a la línia de backports



* Si no s'instal·la sol després d'instal·lar un kernel més nou instal·lem alguns firmwares extres com per exemple:

firmware-iwlwifi (vigilant que la versió no sigui antiga, del kernel antic)
firmware-misc-nonfree
firmware-realtek

+ tweaks -> perquè funcioni el botó dret
+ natural scrolling ?




**Continuarem amb la instal·lació. ÉS necessita configurar els backports perquè
funcioni la gràfica.**
