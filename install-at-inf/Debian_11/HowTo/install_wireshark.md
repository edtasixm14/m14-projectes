# Instal·lació de wireshark a Debian


### Instal·lació del paquet

Com a root (o afegint sudo)

```
apt-get install wireshark
```

Quan ens surti la pantalla de configuració del paquet `wireshark-common` i ens
preguntin:

> _Should non-superusers be able to capture packets?_ 

O sigui
> _els que no siguin super-usuaris haurien de ser capaços de capturar paquets?

Seleccionem `Yes` si és el comportament que volem, clar


### Incorporació del nostre usuari al grup wireshark

Si hem respost `Yes`a la pregunta anterior es crea un grup nou anomenat
`wireshark`. De manera que tot membre d'aquest grup podrà capturar paquets.

Afegim el nostre usuari al grup wireshark perquè pugui capturar paquets:

```
usermod -a -G wireshark el_nostre_usuari
```
(l'ordre anterior s'ha d'executar com a root)

### Reinici del sistema

Tot i que en principi perquè el sistema se n'adoni de que tenim un grup nou
seria suficient amb un logout, de vegades queden alguns processos pendents de
l'usuari que acaba de fer logout i això impedeix la correcta incorporació del
nou grup.

Millor fer un reboot.


### Troubleshooting

Si ens despistem i escollim `No` a la configuració de qui pot capturar paquets
podem sempre tornar a configurar amb l'ordre:

```
dpkg-reconfigure wireshark-common
```

