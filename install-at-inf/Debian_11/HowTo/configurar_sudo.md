## Com fer que un usuari sigui sudo (Super User DO)


Per poder executar algunes ordres com a administrador, podem fer servir la directiva `sudo` davant de l'ordre en qüestió. Aquesta ordre només la podem fer servir si el nostre usuari es troba de manera implícita (el nom del nostre usuari) o explícita (el nom d'un grup al qual pertany el nostre usuari) al fitxer `/etc/sudoers`.
                       

### Comprovació de la pertinença al grup sudo

Executem:
```
groups
```
si un dels grups és _sudo_ (o a Fedora _wheel_) ja podem fer ús d'aquest _superpoder_.

### Com afegir un usuari a sudo

A Debian existeix un grup que ja es troba a `/etc/sudoers` per poder fer
`sudo` i es diu _sudo_ (hi ha d'altres distribucions com Fedora/RedHat a on el
grup es diu _wheel_).

Afegim el nostre usuari al grup _sudo_. Suposem que el nostre usuari es diu _usuari_, com a _root_ fem:

```
adduser usuari sudo
```

Si executem ara l'ordre `groups` sense sortir de la sessió la informació no
estarà actualitzada i el grup _sudo_ no sortirà. De vegades hem de fer un reboot
per assegurar-nos que tots els processos del nostre usuari han acabat.


### Ús de sudo

La forma habitual de treballar amb `sudo` és:

```
sudo ordre
```
La 1a vegada que s'executa surt un missatge més complet que la resta de vegades.


Una altra opció, que es pot deshabilitar, i que només s'hauria de fer servir si
és necessari és convertir-se en root mitjançant `sudo`:

```
sudo -i
```  







### LINKS
+ [Documentació de Debian](https://wiki.debian.org/sudo)

![acudit xkcd de sudo](imatges/sandwich.png)
