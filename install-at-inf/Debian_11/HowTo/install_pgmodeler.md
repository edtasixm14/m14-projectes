# pgmodeler: instal·lació i configuració

## Instal·lació a Debian estable

Executem com a root l'ordre:
```
# apt-get install pgmodeler
```

## Creació de la llençadora (launcher) de pgmodeler a GNOME

Per a poder fer executar `pgmodeler` sense necessitat d'obrir una consola necessitem tenir un fitxer d'extensió desktop al directori `/usr/share/applications/`:

Com a root creem i editem el fitxer `/usr/share/applications/pgmodeler.desktop` i afegim el següent text:

```
[Desktop Entry]
Encoding=UTF-8
Name=pgmodeler
Exec=/usr/bin/pgmodeler
Icon=/usr/share/pgmodeler/conf/pgmodeler_dbm.png
Type=Application
Categories=Application;Development;
MimeType=text/html
Comment=Database Design tool for PostgreSQL
Keywords=database;design;db;sql;query;modeler;administration;development;
```

Desem i sortim. Ja podem escriure des de gnome `pgmodeler`


## Configuració de la nostra base de dades

pgmodeler és una eina que ens ajuda a crear/dissenyar bases de dades, crear imatges del diagrama dissenyat, generar l'script SQL que conté les instruccions de creació de la base de dades i altres `features`. 

Però pgmodeler també ens permet fer l'operació inversa, és a dir, a partir d'una base de dades existent crear el diagrama relacional associat.

Anem a configurar el nostre entorn perquè pgmodeler pugui fer aquesta última operació.


Editem `/etc/postgresql/13/main/pg_hba.conf` per poder atacar per TCP/IP la nostra màquina local:

```
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
```

Reiniciem postgresql:

```
# systemctl restart postgresql
```

Si el nostre usuari no té contrasenya al nostre sistema gestor de base de dades li posem:

```
$ su  -l postgres
Password: 
$ psql template1
...
template1=# ALTER USER el_vostre_usuari PASSWORD 'el_meu_password';
ALTER ROLE
```

Executem pg modeler i anem a *Settings*. Creem una nova connexió local amb les nostres dades:

![Configuració de la connexió](imatges/pgmodeler_settings_connexio.png)


