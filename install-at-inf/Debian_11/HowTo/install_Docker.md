# HowTo: instal·lació de Docker a Debian

## Desinstal·lació versions antigues

Per si de cas existeixen versions antigues (docker, docker.io, docker-engine)
les eliminem:

```
sudo apt-get remove docker docker-engine docker.io containerd runc
```

## Instal·lació i configuració del repositori

Utilitzem mètode d'instal·lació per repositori perquè el sistema actualitzi
automàgicament docker.

Actualització de tot el repositori Debian:

```
sudo apt-get update
```

Instal·lem eïenes necessàries per fer servir el repositori de Dcoker sobre
_https_

```
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```

Afegim la clau oficial de Docker:

```
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```

Finalment afegim el repositori Docker (versió _stable_):

```
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

## Instal·lació del paquet Docker

Actualització de tot el repositori Debian:

```
sudo apt-get update
```

Instal·lem els paquets:

```
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

## Verificació de la instal·lació

Comprovem que la instal·lació ha funcionat bé executant:

```
sudo docker run hello-world
```

Ens mostrarà un missatge de tipus:

```
Unable to find image 'hello-world:latest' locally   <- NO troba aquesta imatge (normal!)
latest: Pulling from library/hello-world			<- La baixa. Ara si que la tenim en local
...
Hello from Docker!
This message shows that your installation appears to be working correctly.
...
```

## Links

+ [Enllaç en el qual es basa aquesta guia](https://docs.docker.com/engine/install/debian/)
+ [Postinstal·lació](https://docs.docker.com/engine/install/linux-postinstall/)

