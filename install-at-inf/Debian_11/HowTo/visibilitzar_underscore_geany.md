# Com visibilitzar l'underscore a geany

## Problema

No es veu l'underscore o guió baix al geany.

Només s'han d'afegir aquestes dues línies al final del fitxer `filetypes.common`:

```
[styling]
line_height=0;2;
```

## Mode consola

```
echo -e '[styling]\nline_height=0;2;'  >>  ~/.config/geany/filedefs/filetypes.common
```

## Mode gràfic

+ Obrir el geany -> Tools -> Configuration Files -> filetypes.common

+ Enganxar les dues línies esmentades al final del fitxer.

+ Desar


Voilà!

